from django.http import HttpResponse
from urllib.parse import parse_qs, urlparse
from myapp.models import User, Person
from django.shortcuts import render
from django.views import View



import logging
logger = logging.getLogger(__name__)

def index(request):
    return render(request, 'index.html')

def debug(input):
    logger.debug('[DEBUG] %s' % input)
    return input

def test_sql_injection1(request):

    # ruleid: python_django_rule-django-raw-used
    res = User.objects.raw(debug("SELECT * FROM myapp_user WHERE username = '{}'".format(request.GET.get('username', 'testuser2'))))
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case 1: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
    
# request object inline with query
# request object format - request.$W.get(...)
# string interpolation with % operator 
def test_sql_injection2(request):

    # ruleid: python_django_rule-django-raw-used
    res = User.objects.raw("SELECT * FROM myapp_user WHERE username = '%s'" % request.GET.get('username', 'testuser2'))
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case 2: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
   
  
# request object inline with query
# request object format - request.$W.get(...)
# string interpolation with F-Strings
def test_sql_injection3(request):

    # ruleid: python_django_rule-django-raw-used
    res = User.objects.raw(f'SELECT * FROM myapp_user WHERE username = "{request.GET.get("username", "testuser2")}"')
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case 3: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")


# request object inline with query
# request object format - request.$W.get(...)
# string concatenation with + operator
def test_sql_injection4(request):

    # ruleid: python_django_rule-django-raw-used
    res = User.objects.raw("SELECT * FROM myapp_user WHERE username = '" + request.GET.get('username', 'testuser2') + "'")
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case 4: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")

#-------------------------------------------------------------

# request object not inline
# string interpolation with str.format() method
def test_sql_injection5(request):

    uname = request.GET.get('username', 'testuser2')
    query = "SELECT * FROM myapp_user WHERE username = '{}'".format(uname)
    # ruleid: python_django_rule-django-raw-used
    res = User.objects.raw(query)
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case 5: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
    
# request object not inline
# string interpolation with % operator 
def test_sql_injection6(request):

    uname = request.GET.get('username', 'testuser2')
    query = "SELECT * FROM myapp_user WHERE username = '%s'" % uname
    # ruleid: python_django_rule-django-raw-used
    res = User.objects.raw(query)
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case 6: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
   
  
# request object not inline
# string interpolation with F-Strings
def test_sql_injection7(request):

    uname = request.GET.get('username', 'testuser2')
    query = f'SELECT * FROM myapp_user WHERE username = "{uname}"'
    # ruleid: python_django_rule-django-raw-used
    res = User.objects.raw(query)
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case 7: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")


# request object not inline
# string concatenation with + operator
def test_sql_injection8(request):

    uname = request.GET.get('username', 'testuser2')
    query = "SELECT * FROM myapp_user WHERE username = '" + uname + "'"
    # ruleid: python_django_rule-django-raw-used
    res = User.objects.raw(query)
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case 8: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
    
#-------------------------------------------------
        

# request object format - request.$W(...)
# string interpolation with str.format() method
def test_sql_injection9(request):

    query_string = urlparse(request.get_full_path()).query
    params = parse_qs(query_string)
    uname = params.get('username', [''])[0]
    # ruleid: python_django_rule-django-raw-used
    res = User.objects.raw("SELECT * FROM myapp_user WHERE username = '{}'".format(uname))
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case 9: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
    
# request object format - request.$W(...)
# string interpolation with % operator 
def test_sql_injection10(request):

    query_string = urlparse(request.get_full_path()).query
    params = parse_qs(query_string)
    uname = params.get('username', [''])[0]
    # ruleid: python_django_rule-django-raw-used
    res = User.objects.raw("SELECT * FROM myapp_user WHERE username = '%s'" % uname)
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case 10: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
   
  
# request object format - request.$W(...)
# string interpolation with F-Strings
def test_sql_injection11(request):

    query_string = urlparse(request.get_full_path()).query
    params = parse_qs(query_string)
    uname = params.get('username', [''])[0]
    # ruleid: python_django_rule-django-raw-used
    res = User.objects.raw(f'SELECT * FROM myapp_user WHERE username = "{uname}"')
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case 11: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")


# request object format - request.$W(...)
# string concatenation with + operator
def test_sql_injection12(request):

    query_string = urlparse(request.get_full_path()).query
    params = parse_qs(query_string)
    uname = params.get('username', [''])[0]
    # ruleid: python_django_rule-django-raw-used
    res = User.objects.raw("SELECT * FROM myapp_user WHERE username = '" + uname + "'")
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case 12: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")  

     
#-------------------------------------------------
    
# request object format - request.$W[...]
# string interpolation with str.format() method
def test_sql_injection13(request):

    uname = request.GET["username"] 
    # ruleid: python_django_rule-django-raw-used
    res = User.objects.raw("SELECT * FROM myapp_user WHERE username = '{}'".format(uname))
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case 13: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
    
# request object format - request.$W[...]
# string interpolation with % operator 
def test_sql_injection14(request):

    uname = request.GET["username"] 
    # ruleid: python_django_rule-django-raw-used
    res = User.objects.raw("SELECT * FROM myapp_user WHERE username = '%s'" % uname)
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case 14: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
   
  
# request object format - request.$W[...]
# string interpolation with F-Strings
def test_sql_injection15(request):

    uname = request.GET["username"] 
    # ruleid: python_django_rule-django-raw-used
    res = User.objects.raw(f'SELECT * FROM myapp_user WHERE username = "{uname}"')
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case 15: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}")


# request object format - request.$W[...]
# string concatenation with + operator
def test_sql_injection16(request):

    uname = request.GET["username"] 
    # ruleid: python_django_rule-django-raw-used
    res = User.objects.raw("SELECT * FROM myapp_user WHERE username = '" + uname + "'")
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case 16: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}")  

#----------------------

# Using Parameterized query - Safe
# True negative case
def test_sql_injection17(request):

    uname = request.GET["username"] 
    # ok: python_django_rule-django-raw-used
    res = User.objects.raw('SELECT * FROM myapp_user WHERE username = %s', (uname,))
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case 17 (Safe): Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 




# Using translations map
# Not using Parameterized queries
def test_sql_injection18(request):

    name_map = {'username': 'person_name', 'email':'person_email'}
    uname = request.GET["username"] 
    # ruleid: python_django_rule-django-raw-used
    res = Person.objects.raw("SELECT * FROM myapp_user WHERE username = '%s'" % uname, translations=name_map)
    try:
        person = next(iter(res))  
        return HttpResponse(f"Test Case 18: Found user!<br>Username: {person.person_name}<br>Email: {person.person_email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 



# Using translations map
# No user input
# True negative
def test_sql_injection19(request):

    name_map = {'username': 'person_name', 'email':'person_email'}

    # ok: python_django_rule-django-raw-used
    res = Person.objects.raw("SELECT * FROM myapp_user", translations=name_map)
    try:
        person = next(iter(res))  
        return HttpResponse(f"Test Case 19 (Safe): Found user!<br>Username: {person.person_name}<br>Email: {person.person_email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}")  
        

# Using translations map
# Using Parameterized queries - Safe
# True negative
def test_sql_injection20(request):

    name_map = {'username': 'person_name', 'email':'person_email'}
    # ok: python_django_rule-django-raw-used
    res = Person.objects.raw("SELECT * FROM myapp_user WHERE username = %s LIMIT 1", [request.GET["username"]], translations=name_map)
    try:
        person = next(iter(res))  
        return HttpResponse(f"Test Case 20 (Safe): Found user!<br>Username: {person.person_name}<br>Email: {person.person_email}")
    except StopIteration:
        return HttpResponse("No such user found.")


# With empty parameters as tuple
# Not using Parameterized queries
def test_sql_injection21(request):
    uname = request.GET["username"] 
    res = User.objects.raw(
                # ruleid: python_django_rule-django-raw-used
                "SELECT * FROM myapp_user WHERE username = '%s'" % uname,
                params=(),
                )
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case 21 : Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 
    
# With empty parameters as list
# No User input: String interpolation % operator
# Not using Parameterized queries
def test_sql_injection22(request):
    uname = request.GET["username"]     
    # ruleid: python_django_rule-django-raw-used
    res = User.objects.raw("SELECT * FROM myapp_user WHERE username = '%s'" % uname, [])
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case 22: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

# With empty parameters as list
# No User input
# True Negative
def test_sql_injection23(request):    
    # ok: python_django_rule-django-raw-used
    res = User.objects.raw("SELECT * FROM myapp_user WHERE username = '%s'" % "testuser2", [])
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case 23 (Safe): Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

#----------------------------------------------------

# No User input
# String interpolation % operator 
# True negative case
def test_sql_injection24(request):

    val = "testuser1" 
    # ok: python_django_rule-django-raw-used
    res = User.objects.raw("SELECT * FROM myapp_user WHERE username = '%s'" % val)
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case 24 (Safe): Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

# No User input
# String concatenation + operator 
# True negative case
def test_sql_injection25(request):
    val = "testuser2"
    query = "SELECT * FROM myapp_user WHERE username = '" + val +"'"     
    # ok: python_django_rule-django-raw-used
    res = User.objects.raw(query)
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case 25 (Safe): Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

# No User input
# string interpolation with F-Strings
# True negative case
def test_sql_injection26(request):
    val = "testuser2"
    query = f"SELECT * FROM myapp_user WHERE username = '{val}'"    
    # ok: python_django_rule-django-raw-used
    res = User.objects.raw(query)
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case 26 (Safe): Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

# No User input
# string interpolation with str.format() method
# True negative case
def test_sql_injection27(request):
    val = "testuser2"
    query = "SELECT * FROM myapp_user WHERE username = '{}'".format(val)     
    # ok: python_django_rule-django-raw-used
    res = User.objects.raw(query)
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case 27 (Safe): Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 
    
#----------

# Testing different parameter name
def test_sql_injection27(myrequest):
    uname = myrequest.GET["username"]
    query = "SELECT * FROM myapp_user WHERE username = '{}'".format(uname)     
    # ruleid: python_django_rule-django-raw-used
    res = User.objects.raw(query)
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case 27 : Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 
    
# Testing multiple view arguments
def test_sql_injection28(request, uname):
    query = "SELECT * FROM myapp_user WHERE username = '{}'".format(uname)     
    # ruleid: python_django_rule-django-raw-used
    res = User.objects.raw(query)
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case 28 : Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 







# Testing interprocedural tainted query returned
def build_query(myrequest):
    uname = myrequest.GET["username"]
    query = "SELECT * FROM myapp_user WHERE username = '{}'".format(uname)
    return query

# Testing different parameter name
def test_sql_injection29(myrequest):
    # ruleid: python_django_rule-django-raw-used
    res = User.objects.raw(build_query(myrequest))
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case 27 : Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

class TestSQLInjection30(View):
    # Testing interprocedural tainted query returned
    def build_query(self):
        uname = self.request.GET["username"]
        query = "SELECT * FROM myapp_user WHERE username = '{}'".format(uname)     
        return query

    # Testing different parameter name
    def get(self, myrequest):
        # ruleid: python_django_rule-django-raw-used
        res = User.objects.raw(self.build_query())
        try:
            user = next(iter(res))  
            return HttpResponse(f"Test Case 27 : Found user!<br>Username: {user.username}<br>Email: {user.email}")
        except StopIteration:
            return HttpResponse("No such user found.")
        except Exception as e:
            return HttpResponse(f"An error occurred: {str(e)}")     

from helpers import myquerybuilder

# Testing different parameter name
def test_sql_injection31(myrequest):
    # ruleid: python_django_rule-django-raw-used
    res = User.objects.raw(myquerybuilder(myrequest))
    res = User.objects.raw(myquerybuilder())
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case 27 : Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 